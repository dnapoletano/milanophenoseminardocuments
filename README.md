# Milano Pheno-Seminar Documents

Detailed procedure and documents to be filled in by seminar speakers and guests.

## Getting started

After a speaker has confirmed, we send the usual e-mail, saying that if needed we can book an accommodation for them
at [hotel palazzo delle stelline](https://www.hotelpalazzostelline.it/) if needed and otherwise they can look for something else themselves.
Flights, as well as food and other means of transport are reimbursed separately. In order to do so they have to keep all
the relevant receipts, in original printed form and hand them in to us before leaving. Train tickets as well as boarding
passes need to be present as well as a payment recipt, even if they are digital.
In case they do proceed and book an accommodation for themselves, it has to explained that `Airbnb` cannot be reimbursed and
`booking.com` (or similar) can only be reimbursed if a receipt from the hotel is also produced (the payment receipt from 
booking alone is not enough).

In general if the stay of the guest exceeds two nights
it has to be accounted as a Scientific collaboration (see below) rather than a seminar.

If the speaker has requested that we booked the hotel in their stance, then the first thing to do is to contact 
[hotel palazzo delle stelline](https://www.hotelpalazzostelline.it/) and ask for a room availability. [](mailto:subject:Info%20prenotazione%20stanza%20DATE%20-%20Università%20e%20INFN%20Milano%20Bicocca&body:TEST)

<a href="mailto:info@hotelpalazzostelline.it?subject=Info prenotazione stanza DATE - Università e INFN Milano Bicocca&body=Gentilissimi,%0D%0A%0D%0AScrivo da parte del Dipartimento di Fisica dell'Università degli Studi di Milano-Bicocca.%0D%0AVorremmo prenotare una stanza singola a nome di SPEAKER dal DATE (NNIGHTS notti).%0D%0AIl pagamento verrebbe effettuato direttamente da INFN Milano Bicocca. Vi preghiamo di emettere fattura elettronica intestata direttamente a:%0D%0A%0D%0AIstituto Nazionale di Fisica Nucleare%0D%0ASezione di Milano Bicocca%0D%0APiazza della Scienza 3%0D%0A20126 MILANO%0D%0Ac.f. 84001850589%0D%0Acodice IPA 8F84EO%0D%0A%0D%0ASeguirà nostro numero di pratica da inserire in fattura.%0D%0A%0D%0AResto in attesa di un vostro cortese riscontro su disponibilità e prezzi.%0D%0A%0D%0AGrazie come sempre per l'attenzione.%0D%0A%0D%0ACordiali Saluti">Here is a template email</a>
And once you receive it from either Marilena or Giuseppina, you should send the identification code.

## List of documents to be filled in by us

- [ ] [Richiesta Autorizzazione Spese](https://www.mib.infn.it/main/media/seminari/2022/RICHIESTA_AUTORIZZAZIONE_SPESE.pdf) check the `Rimborso Spese` entry, `Pié di Lista` and `Viaggio` with an estimate or the known ammount, and <ins><bf>only if the hotel is paid by the speaker</bf></ins> `Soggiorno`. Correspondigly <ins><bf> if the hotel is paid by the speaker</bf></ins> the entry `Hotel pagato dall’ospite` must be flagged. Otherwise the field `Soggiorno` must be left empty and the entry `Hotel pagato dall’INFN` must be flagged.

- [ ] [Lettera d'Incarico (italian)](https://www.mib.infn.it/main/media/seminari/2022/LETTERA_INCARICO_1_IT.pdf) or [Lettera d'Incarico (english)](https://www.mib.infn.it/main/media/seminari/2022/LETTERA_INCARICO_1_EN.pdf). This is a fairly straightforward document, unless we want to pay the guest (option `b`) or pay and reimburse (option `c`), we only need to fill the `a` flag. The top part is filled in by us, and signed by the speaker. If the speaker plans to stay longer than 2 nights it should be listed as a `scientific collaboration` otherwise as a seminar. This should be first sent to the INFN administratives which will then send it back to be <bf>physically</bf> signed by the speaker.


## List of documents to be filled in and/or signed by the guest

- [ ] [Inquadramento Fiscale Contributivo (IT only)](Modello_LAO_2023.pdf) Has to be filled in and signed. Note this is <ins><bf>only for italian speakers</ins></bf> (There will be a corresponding one for foreigners, which will avoid having to request a `Codice Fiscale` but not quite yet). 
- [ ] [Inquadramento Fiscale Contributivo (Non-IT)](https://www.mib.infn.it/main/media/seminari/2022/DATI%20EN.pdf) This is <ins><bf>only for foreigner speakers</ins></bf> and will likely be changed soon. At any rates, the most widespread case is that they <bf>have not</bf> earned more than 5K euros in the same fiscal year in Italy coming from the same contract (prestazioni occasionali), I will assume this is the case, otherwise you need to ask exactly what should be done. They have to fill in the header, and flag the `A` option. Then they have to flag, within the `A` section, the option `does not exceed euro 5.000`, and below in the subsection `The undersigned states to be` they must flag the first option, otherwise they need to attach a fiscal declaration from their country of origin. Lastly they need to fill section `D` and sign, attaching a scan of their passport. In the first part, there appears an entry asking for they SSN (`Codice Fiscale`), if they do not possess one at the time they have to fill this one in, there is an additional form they need to sign, which is unfortunately only in italian, so we can ask them to sign it and we can fill it in on their behalf, [Richiesta Codice Fiscale](https://www.mib.infn.it/main/media/seminari/2022/Richiesta%20codice%20fiscale.pdf). It may happen that they already have a Codice Fiscale, but do not remember it, or can't retrieve it. In that case, ask.

## Collect signed documents and send them to have the request approved
All the documents mentioned above (both those to be filled in by us and those to be filled in or signed by the speaker), need to be sent to the <a href="mailto:giuseppina.bianco@mib.infn.it?subject=Seminario SPEAKER&cc=noppadol.mekareeya@mib.infn.it">INFN-delegates</a>. Remember to attach a scanned version of the speaker's passport as well as the seminar poster. They will then proceed to formalise the request, and you will receive a digitally protocolled version of the `Lettera d'Incarico` which will need to be signed by the speaker upon their arrival.
Once the speaker is here we can collect all the receipts, and put them in a folder to be then handed over to Giuseppina Bianco (remember to print every document you already sent as well as an actual Seminar poster). If the Speaker has more receipt after the seminar which cannot be given to you in person (unless they are already digital), they should be sent by mail (physical post).

## Last Bureaucratic step, INFN digital identyity

The last thing the speaker needs to be doing in order to be able to be reimbursed is to have a digital identity with INFN.
Being INFN a public entity, new regulations require all INFN guests to be digitally identified by means of a self-service registration procedure available at the [link](https://userportal.app.infn.it/) 

and followed by a video call.

Once you have registered you will have to:

1) login to the [userportal](https://userportal.app.infn.it/) with your new credentials and you will find your profile data.

2) By selecting in the menu on the left the item "Enabling requests" specify that you want to obtain a certified identity and flag the field Verify identity.
You must declare to have read and accepted the information note on the processing of personal data at INFN.
After pressing the button Next step, choose the office where to carry out your identification (it's a drop-down menu, open it to find the location you want and choose "Sezione di Milano Bicocca");

3)Your digital identity request will be completed once the INFN identity certification office will contact you to perform a "de visu" identification by means of a video call.

Here you can find a [complete guide to the procedure](https://wiki.infn.it/cn/ccr/aai/doc/rid/istruzioni/userportal/user_loa-eng)





